$.fn.selectRange = function(start, end) {
	return this.each(function() {
		if(this.setSelectionRange) {
			this.focus();
			this.setSelectionRange(start, end);
		} else if(this.createTextRange) {
			var range = this.createTextRange();
			range.collapse(true);
			range.moveEnd('character', end);
			range.moveStart('character', start);
			range.select();
		}
	});
};

var wiki = {
	query : 'http://it.wikipedia.org/w/api.php?format=json&action=query&generator=random&grnnamespace=0&prop=extracts&callback=?',
	pages : {},
	page : {},
	title : 'Batman',
	written : '',
	extract : 'Batman è tuo padre.'
};

$('#search').addClass('loading');

$(document).ready(function() {
	$.ajax({
		type: "GET",
		url: wiki.query,
		contentType: "application/json; charset=utf-8",
		async: true,
		dataType: "jsonp",
		headers: {
			'User-Agent': 'CosaCerchi/1.0 (http://www.quotidiano.net/cosacerchi/; ireneo.piccinini@monrif.net)'
		},
		success: function(data, textStatus, jqXHR) {
			wiki.pages = data.query.pages;
			for(var pid in wiki.pages) {
				wiki.page = wiki.pages[pid];
				wiki.title = wiki.page.title;
				wiki.extract = wiki.page.extract;
				break;
			}
			$('#search').removeClass('loading');
			$('#q').blur();
			$('#footer').text('"è un motore di ricerca fascista e anche un po\' nazista" -\u00a0Bruma\u00a02000');
		},
		error: function(errorMessage) {
		}
	});
	$('#search').empty();
	$('<form id="searchform"></form>').appendTo('#search');
	$('<input type="text" id="q" name="q" value="" />').appendTo('#searchform');
	$('<div id="lookn4"></div>').appendTo('#searchform');
	$('<button type="submit" id="s">Cerca con Cosa cerchi</button>').appendTo('#searchform');
	$('<button type="button" id="f">Mi sento fortunato</button>').appendTo('#searchform');
	$('#searchform').submit(function(event){
		var uvalue = $('#q').val();
		if(uvalue == wiki.title) wikifill();
		event.preventDefault();
		return false;
	});
	$('#f').click(function(){ wikifill(); });
	$('#s').click(function(event){
		if($('#q').val().length > 0) wikifill();
		else $('#q').focus();
		event.preventDefault();
	});
	$('#q').focus(function(event){
		$('#lookn4').empty();
		$('#lookn4').html('<strong>Forse cercavi:</strong> <a>' + wiki.title + '</a> ?');
		$('#lookn4 a').click(function(event){ wikifill(); });
	});
	$('#q').keydown(function(event){
		if(event.which < 1 || event.which > 47) {
			if(wiki.written.length < wiki.title.length) {
				wiki.written += wiki.title[wiki.written.length];
				$('#q').val(wiki.written);
				if(wiki.written.length > 4) {
					var r0 = wiki.written.length;
					var r1 = wiki.title.length;
					$('#q').val(wiki.title).selectRange(r0, r1);
				}
			}
			event.preventDefault();
			return false;
		}
	});
});

function wikifill() {
	$('#result').empty();
	$('#result').addClass('loaded');
	$('#q').val(wiki.title);
	$('<h1 id="title"></h1>').text(wiki.title).appendTo('#result');
	$('<p id="extract"></p>').html(wiki.extract).appendTo('#result');
}
